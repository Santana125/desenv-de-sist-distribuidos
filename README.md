## Desenvolvimento de Sistemas Distribuídos

Material de aula desenvolvido para a disciplina de sistemas distribuídos, ministrada pelo professor Msc. Tiago Arruda
na UNIP - Sorocaba.

* Semestre letivo: 02/2019
* Turmas: CC3P17 e CC4P17

1. Primeira parte:
	1. [x] Exemplo de soma, utilizando **RMI**
	2. [ ]  Exemplo utilizando **CORBA**.
	3. [ ] Script contendo os passos para utilização de **NFS**
	4. [x] Material de Aula

2. Segunda parte:
	1. [ ] Exemplo utilizando **WebServices**

---