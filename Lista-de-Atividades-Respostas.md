### DESENVOLVIMENTO DE SISTEMAS DISTRIBUIDOS

**Nome:** Gustavo Felipe de Santana **RA:** D00HBC7 **Turma:**CC8P17 


1. **Quais as características principais da computação móvel e ubíqua?**


   R: **Móvel:**Portabilidade e conexão com redes em lugares diferentes.
       **Ubíqua:**Acesso onipresente a serviços de computação.

       
2. **Descreva alguns cenários do dia-a-dia em que se pode identificar a computação 
pervasiva.**


   R: Smartphones, painel de carros, displays e controles em eletrodomésticos.

   
3. **Descreva as formas que a WEB pode se utilizar para compartilhar recursos com seus usuários. Justifique.**


   R: Através de servidores que estão conectados na WEB podem ser utilizados para compartilhar recursos como programas, mídias e documentos. Por meio da URL é possível identificar qual tipo de recurso será compartilhado.

   
4. **Você foi contratado para implementar um serviço web, que deve disponibilizar as seguintes operações:**


a. Verificar se determinado cpf é de uma pessoa adimplente ou inadimplente; 


      R: https://www.consultacpf.net/inadimplencia?cpf=\<cpf\>
   
   
      Ex: https://www.consultacpf.net/inadimplencia?cpf=34092352430
   
   
b. Verificar se o cpf informado possui alguma passagem pela cadeia;
   
   
      R: https://www.consultacpf.net/acriminal?cpf=\<cpf\>
    
    
      Ex: https://www.consultacpf.net/acriminal?cpf=34092352430
   
   
c. Verificar se determinado cep informado corresponde à cidade informada; 
   
   
      R: https://www.consultacpf.net/checarcep?cep=\<cep\>
    
    
      Ex:https://www.consultacpf.net/checarcep?cep=18120000
