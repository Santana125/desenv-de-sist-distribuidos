
package servercpfrmi;

import interfaces.Operacoes;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.InputMismatchException;

public class ServerCPFRMI implements Operacoes{


    public static void main(String[] args) {
        try {
            /* REGISTRA O SERVICO NA PORTA 1099 */
            Registry registro = LocateRegistry.createRegistry(2099);

            ServerCPFRMI serverCPF = new ServerCPFRMI();

            Operacoes operacao = (Operacoes) UnicastRemoteObject.exportObject(serverCPF, 0);

            registro.bind("Operacao", operacao);

            System.err.println("Em execução");
            
        } catch (Exception ex) {
            System.err.println("Erro: " + ex.getMessage());
            ex.printStackTrace();
        }
    
    }

    @Override
    public boolean checarCPF(String CPF) throws RemoteException {
        boolean isValid = true;
        char dig10, dig11;
        int sm, i, r, num, peso;
        try {
            sm = 0;
            peso = 10;
            for (i=0; i<9; i++) {                      
            num = (int)(CPF.charAt(i) - 48); 
            sm = sm + (num * peso);
            peso = peso - 1;
            }
          
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char)(r + 48); 
            sm = 0;
            peso = 11;
            for(i=0; i<10; i++) {
            num = (int)(CPF.charAt(i) - 48);
            sm = sm + (num * peso);
            peso = peso - 1;
            }
          
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                 dig11 = '0';
            else dig11 = (char)(r + 48);
          
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                 return(true);
            else return(false);
                } catch (InputMismatchException erro) {
                return(false);
            }
        }
}
