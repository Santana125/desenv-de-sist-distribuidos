
package clientecpfmi;

import interfaces.Operacoes;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class ClienteCPFRMI {
    public static void main(String[] args) {
        boolean cpf;
        try{
            Registry registro = LocateRegistry.getRegistry("127.0.0.1", 2099);
        
            Operacoes operacao = (Operacoes)registro.lookup("Operacao");
            
            cpf = operacao.checarCPF("83018173058");
            
            if (cpf)
                System.err.println("CPF é Valido");
            else
                System.err.println("CPF é Invalido ");
            
        
        } catch(Exception ex) {
            System.err.println("Erro: " + ex.toString());
        }
    }
}
