
package clientecpfmi;

import interfaces.Operacoes;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;


public class ClienteCompletoRMI {
    public static void main(String[] args) {
        boolean isCpf;
        int op;
        Scanner scanner = new Scanner (System.in);
        try{
            Registry registro = LocateRegistry.getRegistry("127.0.0.1", 2099);
        
            Operacoes operacao = (Operacoes)registro.lookup("Operacao");
            
            System.out.print("Escolha: \n1) Soma\n2)Operação\n3)Validar CPF\n");  
            op = scanner.nextInt();
            
            switch(op){
            
                case 1: {
                    int a,b;
                    System.out.print("Insira o Primeiro valor: ");
                    a = scanner.nextInt();
                    System.out.print("Insira o Segundo valor: ");
                    b = scanner.nextInt();
                    
                    System.err.println("Soma = " + operacao.soma(a, b));
                    break;
                }
                case 2: {
                    float x;
                    System.out.print("Insira um valor: ");
                    x = scanner.nextFloat();
                    System.err.println("Calculo = " + operacao.calculo(x));
                    break;
                }
                case 3: {
                    System.out.print("Insira um CPF: ");
                    String cpf = scanner.next();
                    isCpf = operacao.checarCPF(cpf);
                    if (isCpf)
                        System.err.println("CPF é Valido");
                    else
                        System.err.println("CPF é Invalido ");
                    break;
                }
                default: {
                    System.err.println("Nenhuma Opção selecionada. ");
                    break;
                }
            }
            
        
        } catch(Exception ex) {
            System.err.println("Erro: " + ex.toString());
        }
    }
}
