/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientesomarmi;

import interfaces.Operacoes;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 *
 * @author Tiago Arruda
 */
public class ClienteSomaRMI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try{
            Registry registro = LocateRegistry.getRegistry("127.0.0.1", 2099);
        
            Operacoes operacao = (Operacoes)registro.lookup("Operacao");
            
            System.err.println("Soma = " + operacao.soma(5, 10));
            System.err.println("Calculo = " + operacao.calculo(10f));
        
        } catch(Exception ex) {
            System.err.println("Erro: " + ex.toString());
        }
    }
}
