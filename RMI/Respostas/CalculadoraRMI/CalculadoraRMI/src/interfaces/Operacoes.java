package interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Operacoes extends Remote{
    int soma(int a, int b) throws RemoteException;
    float calculo(float x) throws RemoteException;
}
