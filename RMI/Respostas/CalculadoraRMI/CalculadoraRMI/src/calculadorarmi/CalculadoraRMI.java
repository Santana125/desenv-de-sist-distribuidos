package calculadorarmi;

import interfaces.Operacoes;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class CalculadoraRMI implements Operacoes{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            /* REGISTRA O SERVICO NA PORTA 1099 */
            Registry registro = LocateRegistry.createRegistry(2099);

            CalculadoraRMI calculadora = new CalculadoraRMI();

            Operacoes operacao = (Operacoes) UnicastRemoteObject.exportObject(calculadora, 0);
            

            registro.bind("Operacao", operacao);

            System.err.println("Em execução");
            
        } catch (Exception ex) {
            System.err.println("Erro: " + ex.getMessage());
            ex.printStackTrace();
        }
    
    }

    @Override
    public int soma(int a, int b) throws RemoteException {
        return a + b;
    }
    @Override
    public float calculo(float x) throws RemoteException {
        return x*x + x*4 - 2;
    }
}
