/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoracorba;

import CalculadoraApp.*;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

/**
 *
 * @author Tiago Arruda
 */
public class CalculadoraCORBA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Cria e inicializa o ORB (Object Request Broker)
            ORB orb = ORB.init(args, null);

            // Ativa o adaptador de objeto (POAManager)
            POA rootpoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootpoa.the_POAManager().activate();

            // Cria um servente e o registra com o ORB
            CalculadoraAppImpl calculadoraImpl = new CalculadoraAppImpl();
            calculadoraImpl.setORB(orb);

            // obtêm referência de objeto do servente
            org.omg.CORBA.Object ref = rootpoa.servant_to_reference(calculadoraImpl);
            Calculadora href = CalculadoraHelper.narrow(ref);

            // obtem referencia do serviço, com nome "NameService"
            org.omg.CORBA.Object objRef
                    = orb.resolve_initial_references("NameService");            
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // Vincula a referência de objeto a um nome (Hello)
            String name = "Calculadora";
            NameComponent path[] = ncRef.to_name(name);

            /* REGISTRA O OBJETO REMOTO - DE NOME "Calculadora", E DISPONIBILIZA NO SERVICO */
            ncRef.rebind(path, href);

            System.out.println("Servidor em execução");

            // aguarda por invocações dos clientes
            orb.run();
            
        } catch (Exception e) {
            System.err.println("ERROR: " + e);
            e.printStackTrace(System.out);
        }

        System.out.println("HelloServer Exiting ...");

    }
}
