package calculadoracorba;

import CalculadoraApp.CalculadoraPOA;
import org.omg.CORBA.ORB;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TIAGO ARRUDA
 */
public class CalculadoraAppImpl extends CalculadoraPOA {

    private ORB orb;

    public void setORB(ORB orb_val) {
        orb = orb_val;
    }

    @Override
    public int soma(int a, int b, int c) {
        return a + b + c;
    }

    @Override
    public void shutdown() {
         orb.shutdown(false);
    }
}
